from scanbot.utils import px, make_div
from .base import BaseConfig, BaseComponent
from .BubbleConfig import default_bubble_config
from math import ceil


class MCQuestionConfig(BaseConfig):
    default = {
        'height': 16,
        'label_width': 35,
        'x_margin': 3,
        'option_list': ['A', 'B', 'C', 'D', 'E'],
        'bubble_config': default_bubble_config,
    }
    required = ['label']


class MCQuestion(BaseComponent):
    '''Multiple Choice Question component'''
    config_class = MCQuestionConfig

    def bubble_x(self, index):
        '''Compute X coord for bubble at index INDEX'''
        return (
            self.x +
            self.config.label_width +
            index * (
                self.config.x_margin +
                self.config.bubble_config.width
            )
        )

    def bubble_y(self):
        '''Compute Y coord for all bubbles'''
        y_offset = self.config.bubble_config.height - self.config.height

        return self.y - (y_offset / 2)

    @property
    def html(self):
        html = make_div(
            self.config.label,
            self.x, self.y,
            self.config.label_width,
            self.config.height,
            {
                'vertical-align': 'middle',
                'line-height': '{}pt'.format(self.config.height)
            }
        )
        y = self.bubble_y()

        # Iterate through the bubbles and make circles
        for i, option in enumerate(self.config.option_list):
            x = self.bubble_x(i)
            html += make_div(
                option,
                x, y,
                self.config.bubble_config.width,
                self.config.bubble_config.height,
                self.config.bubble_config.style,
            )
        return html

    @property
    def aoi(self):
        y = self.bubble_y()
        return {self.ref_id: {
            'type': 'group',
            'items': {
                'option_{}'.format(option): {
                    'type': 'rect',
                    'x': px(self.bubble_x(i)),
                    'y': px(y),
                    'w': px(self.config.bubble_config.width),
                    'h': px(self.config.bubble_config.height),
                }
                for i, option in enumerate(self.config.option_list)
            }
        }}

    def mark(self, extract, thresh=0.4):
        thisguy = getattr(extract, self.ref_id)
        mark = {
            self.ref_id: {
                'selections': [
                    x for x in self.config.option_list if
                    getattr(thisguy, 'option_{}'.format(x)).white_level < thresh
                ]
            }
        }
        return (mark, {})


class MCQuestionGridConfig(BaseConfig):
    default = {
        'num_questions': 100,
        'questions_per_col': 20,
        'y_margin': 4,
        'col_margin': 30,
        'question_config': MCQuestionConfig(),
        'question_offset': 0,
    }
    required = ['ref_id_template', 'label_template']


class MCQuestionGrid(BaseComponent):
    '''Creates a grid of questions using the same settings'''
    config_class = MCQuestionGridConfig

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Helper variables
        column_width = (
            self.config.col_margin +
            self.config.question_config.label_width +
            len(self.config.question_config.option_list) * (
                self.config.question_config.bubble_config.width +
                self.config.question_config.x_margin
            )
        )
        row_height = self.config.question_config.height

        self.questions = []
        index_in_col = 0 
        cur_col = 0
        for q in range(self.config.num_questions):
            x = self.x + column_width * cur_col
            y = self.y + row_height * index_in_col
            ref = self.config.ref_id_template.format(self.config.question_offset + q + 1)
            label = self.config.label_template.format(self.config.question_offset + q + 1)
            self.questions.append(
                MCQuestion(
                    ref, x, y,
                    {
                        'base': self.config.question_config,
                        'label': label
                    }
                )
            )
            index_in_col += 1
            if index_in_col >= self.config.questions_per_col:
                cur_col += 1
                index_in_col = 0

    @property
    def html(self):
        return ''.join([x.html for x in self.questions])

    @property
    def aoi(self):
        aoi = {}
        for x in self.questions:
            aoi.update(x.aoi)
        return aoi

    def mark(self, extract, thresh=0.5):
        marks = {}
        for x in self.questions:
            marks.update(x.mark(extract, thresh=thresh)[0])
        return (marks, {})

