from scanbot.utils import px, make_div
from .base import BaseConfig, BaseComponent


class HTMLBlockConfig(BaseConfig):
    default = {
        'style': {},
        'capture': False,
    }
    required = ['content', 'width', 'height']


class HTMLBlock(BaseComponent):
    '''General use HTML block. Can be flagged to be captured'''
    config_class = HTMLBlockConfig

    @property
    def html(self):
        return make_div(
            self.config.content,
            self.x, self.y,
            self.config.width, self.config.height,
            self.config.style
        )

    @property
    def aoi(self):
        if self.config.capture:
            return {
                self.ref_id: {
                    'type': 'rect',
                    'x': px(self.x),
                    'y': px(self.y),
                    'w': px(self.config.width),
                    'h': px(self.config.height),
                }
            }
        return {}

    def mark(self, extract, thresh=0.1):
        if self.config.capture:
            thisguy = getattr(extract, self.ref_id)
            images = {self.ref_id: thisguy.data}
        else:
            images = {}
        return ({}, images)
