from .base import BaseConfig


class BubbleConfig(BaseConfig):
    default = {
        'width': 8,
        'height': 12,
        'style': {
            'text-align': 'center',
            'border': '0.6pt solid black',
            'border-radius': '9pt',
            'color': '#000000',
            'box-sizing': 'border-box',
            'line-height': '11pt',
            'font-size': '5pt',
        }
    }


default_bubble_config = BubbleConfig()
