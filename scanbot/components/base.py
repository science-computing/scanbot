def deepmerge(d1, d2):
    '''Deeply merges two dictionaries recursively. Very inefficient'''
    if isinstance(d1, dict):
        new = dict(d1)
        for key, val in d2.items():
            if isinstance(val, dict) and key in new:
                if isinstance(new[key], BaseConfig):
                    new[key] = new[key].__class__(base=new[key], **val)
                new[key] = deepmerge(new[key], val)
            else:
                new[key] = val
        return new
    elif isinstance(d1, BaseConfig):
        new = d1.__class__(base=d1, **d2)
        return new


class BaseConfig:
    default = {}
    required = []

    def __init__(self, base=None, **kwargs):
        if base is not None:
            self._values = deepmerge(base._values, kwargs)
        else:
            self._values = deepmerge(self.default, kwargs)

    def _check_errors(self):
        for r in self.required:
            if r not in self._values:
                raise Exception('Missing config setting "{}"'.format(r))

    def _recurse_errors(self):
        for key, val in self._values.items():
            if isinstance(val, BaseConfig):
                val._check_errors()

    def __getattr__(self, attr):
        if not attr.startswith('_'):
            return self._values[attr]
        else:
            return getattr(self, attr)


class BaseComponent:
    '''A basic component others will be derived from'''

    config_class = BaseConfig

    def __init__(self, ref_id, x, y, config):
        self.ref_id = ref_id
        self.x = x
        self.y = y
        if isinstance(config, self.config_class):
            self.config = config
        elif isinstance(config, dict):
            self.config = self.config_class(**config)
        elif config is None:
            self.config = self.config_class()
        self.config._check_errors()

    @property
    def html(self):
        raise NotImplementedError

    @property
    def aoi(self):
        raise NotImplementedError
