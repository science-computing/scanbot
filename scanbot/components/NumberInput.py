from scanbot.utils import px, make_div
from .base import BaseConfig, BaseComponent
from .BubbleConfig import default_bubble_config


class NumberInputConfig(BaseConfig):
    default = {
        'box_height': 25,
        'box_width': 18,
        'number_bottom_margin': 4,
        'y_margin': 4,  # Margin between bubbles
        'box_style': {
            'border': '0.6pt solid black',
            'box-sizing': 'border-box',
        },
        'num_digits': 8,
        'digits': [x for x in range(10)],
        'bubble_config': default_bubble_config

    }
    required = ['num_digits']


class NumberInput(BaseComponent):
    config_class = NumberInputConfig

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bubble_left_padding = (
            self.config.box_width - self.config.bubble_config.width
        ) / 2

    def bubble_x(self, i):
        return self.x + self.config.box_width * i + self.bubble_left_padding

    def bubble_y(self, j):
        return self.y + self.config.box_height + j * (self.config.bubble_config.height + self.config.y_margin) + self.config.y_margin / 2 + self.config.number_bottom_margin

    @property
    def html(self):
        html = ''
        for i, box in enumerate(
            range(self.config.num_digits)
        ):
            html += make_div(
                '',
                self.x + self.config.box_width * i,
                self.y,
                self.config.box_width,
                self.config.box_height,
                self.config.box_style,
            )
            for j, option in enumerate(self.config.digits):
                html += make_div(
                    option,
                    self.bubble_x(i),
                    self.bubble_y(j),
                    self.config.bubble_config.width,
                    self.config.bubble_config.height,
                    self.config.bubble_config.style,
                )
        return html

    @property
    def aoi(self):
        aoi = {}
        aoi['manual'] = {
            'type': 'rect',
            'x': px(self.x),
            'y': px(self.y),
            'w': px(self.config.box_width * self.config.num_digits),
            'h': px(self.config.box_height)
        }
        for digit_i, digit in enumerate(range(self.config.num_digits)):
            digit = {'type': 'group', 'items': {}}
            for option_i, option in enumerate(self.config.digits):
                digit['items']['option_{}'.format(option)] = {
                    'type': 'rect',
                    'x': px(self.bubble_x(digit_i)),
                    'y': px(self.bubble_y(option_i)),
                    'w': px(self.config.bubble_config.width),
                    'h': px(self.config.bubble_config.height),
                }
            aoi['digit_{}'.format(digit_i)] = digit
        return {self.ref_id: {'type': 'group', 'items': aoi}}

    def mark(self, extract, thresh=0.5):
        thisguy = getattr(extract, self.ref_id)
        images = {self.ref_id: thisguy.manual.data}
        mark = {self.ref_id: {'number': None}}
        st = ''
        for digit_i, digit in enumerate(range(self.config.num_digits)):
            d = getattr(thisguy, 'digit_{}'.format(digit))
            thresholds = [
               getattr(d, 'option_{}'.format(x)).white_level
               for x in self.config.digits
            ]
            selections = [
                x for i, x in enumerate(self.config.digits) if
                thresholds[i] < thresh
            ]
            if len(selections) == 0:
                # If we determine a number is blank, bail
                mark[self.ref_id]['error'] = "Could not extract number"
                return (mark, images)
            if len(selections) > 1:
                # Grab the lowest number if we had multiple
                selections = [self.config.digits[thresholds.index(min(thresholds))]]
            st += str(selections[0])
        mark[self.ref_id]['number'] = st
        return (mark, images)
