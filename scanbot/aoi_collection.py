import os
import cv2


class AOIGlyph:
    """Not currently implemented"""

    def __init__(self):
        pass

class AOIObject:
    def __init__(self, config, parent_collection):
        self.parent = parent_collection
        self.config = config
        self.error = None

    def _get_data(self, img):
        x = self.config["x"]
        y = self.config["y"]
        w = self.config["w"]
        h = self.config["h"]
        return img[y : y + h, x : x + w]

    @property
    def data(self):
        return self._get_data(self.parent.image)

    @property
    def processed_data(self):
        return self._get_data(self.parent.processed)

    @property
    def white_level(self):
        if self.error:
            return 1.0
        if not hasattr(self, "_threshold"):
            p = self.processed_data
            self._threshold = 1.0 * cv2.countNonZero(p) / p.size
        return self._threshold


class AOICollection:
    # Parent and root because i needed to keep the api stable...
    def __init__(self, image, aoidef, root=False, parent_collection=None):
        self.parent = parent_collection
        if root:
            self._image = image
            blurred = cv2.GaussianBlur(image, (5, 5), cv2.BORDER_DEFAULT)
            # Need to convert back to grayscale here
            # There's definitely some optimization potential here
            t = cv2.threshold(
                cv2.cvtColor(blurred, cv2.COLOR_BGR2GRAY),
                0,
                255,
                cv2.THRESH_BINARY + cv2.THRESH_OTSU,
            )[1]
            self._processed = t

        self._registered = []
        for label, val in aoidef.items():
            if val["type"] in ["rect", "circle"]:
                data = None
                error = None
                try:
                    x = val["x"]
                    y = val["y"]
                    w = val["w"]
                    h = val["h"]
                    data = processed[y : y + h, x : x + w]
                except:
                    error = "Could not extract data"
                self._registered.append(label)
                setattr(
                    self, str(label),
                    AOIObject(
                        val,
                        parent_collection = self.parent if not root else self
                    )
                )
            elif val["type"] == "glyph":
                pass
            elif val["type"] == "group":
                self._registered.append(label)
                setattr(self, label, AOICollection(
                    image,
                    val["items"],
                    parent_collection = self.parent if not root else self
                    )
                )
    @property
    def image(self):
        if not self.parent:
            return self._image
        return self.parent.image

    @property
    def processed(self):
        if not self.parent:
            return self._processed
        return self.parent.processed

    def dump_to_folder(self, path, prefix=""):
        """Creates a folder and dumps all of the extracted images into it"""
        if not os.path.exists(path):
            os.makedirs(path)
        for x in self._registered:
            aoi = getattr(self, x)
            if isinstance(aoi, AOICollection):
                aoi.dump_to_folder(path, prefix="{}{}__".format(prefix, x))
            elif isinstance(aoi, AOIObject):
                if aoi.data is not None:
                    cv2.imwrite(
                        os.path.join(path, "{}{}.{}".format(prefix, x, "jpg")),
                        aoi.data,
                    )
