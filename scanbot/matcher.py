from __future__ import print_function
import cv2
import numpy as np
import math
from .aoi_collection import AOICollection
from .mark_result import MarkResult


def outlier_indices(array, factor):
        """Gets the index of any array elements which
        are outliers"""
        a = np.array(array)
        return [
            i for i, x in enumerate(
                abs(a - np.mean(array)) > factor*np.std(a)
            ) if x
        ]


def angle_between(v1, v2):
    v1_u = v1 / np.linalg.norm(v1)
    v2_u = v2 / np.linalg.norm(v2)
    # Returns NON-angle. To get angle just arrcos this
    return np.clip(np.dot(v1_u, v2_u), -1.0, 1.0)


def get_black_pixel_ratio(bw_image, polygon):
    """
    New version of this function, instead of masking the whole
    giant image we mask individual regions of interest.
    It was very difficult to wrangle numpy's funky syntax
    """
    x = polygon[:, 0][:, 0]
    y = polygon[:, 0][:, 1]
    min_x = min(x)
    max_x = max(x)
    min_y = min(y)
    max_y = max(y)

    # Extract the region...
    roi = bw_image[min_y:max_y, min_x:max_x]
    mask = np.zeros_like(roi)

    p = polygon - np.array([
        min_x, min_y
    ], dtype=np.int32)

    mask = np.zeros_like(roi)
    cv2.fillPoly(mask,[p],(255))
    scan = roi.copy()

    scan[mask==0] = 255
    black_pixels = np.sum(scan<=200)

    # Now cover the polygon in black and re-count for ALL pixels in polygon
    scan[mask==255] = 0
    total_pixels = np.sum(scan==0)
    return black_pixels / total_pixels


def _OLD_get_black_pixel_ratio(bw_image, polygon):
    """
    Old mechanism that masked the whole page
    """
    # Make a mask of our polygon
    mask = np.zeros_like(bw_image)
    cv2.fillPoly(mask,[polygon],(255))

    # Make everything EXCEPT our polygon white
    # then we can count the black pixels 
    scan = bw_image.copy()
    scan[mask==0] = 255
    black_pixels = np.sum(scan<=200)

    # Now cover the polygon in black and re-count for ALL pixels in polygon
    scan[mask==255] = 0
    total_pixels = np.sum(scan==0)
    return black_pixels / total_pixels

def get_bounding_rect(gray):

    # Old method
    # brighten, blur, thresh (bw)
    #kernel = np.ones((4, 4), np.uint8)
    #dilation = cv2.dilate(gray, kernel, iterations=1)
    #blur = cv2.GaussianBlur(dilation, (5, 5), 0)
    #thresh = cv2.adaptiveThreshold(blur, 255, 1, 1, 11, 2)

    # New method:
    # Find edges
    edged = cv2.Canny(gray, 30, 250)

    # Get contours
    centers = []
    c = cv2.findContours(
        edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    edged = cv2.GaussianBlur(edged, (5, 5), 0)
    for cnt in c[0]:
        # Approximate polygons
        approx = cv2.approxPolyDP(
            cnt, 0.1 * cv2.arcLength(cnt, True), True)
        # Keep triangles
        area = cv2.contourArea(cnt)
        if len(approx) == 3 and area > 600:
            cv2.drawContours(edged, [approx], -1, (255,255,255))

            # Ensure black threshold is above the ratio we want
            ratio = get_black_pixel_ratio(gray, approx)
            if float(ratio) >= 0.7:  # Value obtained experimentally
                # Find center of triangle
                M = cv2.moments(approx)
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                above = []
                below = []
                for point in approx:
                    if point[0][1] <= cY:
                        above.append(point)
                    else:
                        below.append(point)

                if len(above) == 1:
                    point = above[0]
                    inverted = True
                else:
                    point = below[0]
                    inverted = False
                tilt = math.degrees(math.atan((point[0][1]-cY)/(point[0][0]-cX)))
                centers.append(([cX, cY], area, tilt, inverted, ratio))
                # debug
                # cv2.drawContours(gray, [approx], -1, (255,255,255))
                # cv2.circle(gray, (cX, cY), 4, (255, 0, 0))
    
    # Grab the largest triangles -- I grab the 8 largest
    # Value gained experimentally
    centers = sorted(centers, key=lambda x: -x[1])[:8]

    xy_sum = [sum(p[0]) for p in centers]
    xy_diff = [(p[0][0]-p[0][1]) for p in centers]

    # Determine which triangle is in which corner
    lt = centers[xy_sum.index(min(xy_sum))]
    rt = centers[xy_diff.index(max(xy_diff))]
    lb = centers[xy_diff.index(min(xy_diff))]
    rb = centers[xy_sum.index(max(xy_sum))]

    bound = [lt, rt, lb, rb]

    # Any bad trianles will be marked as [-1 -1]
    bad_triangle = ([-1, -1],)
    tilts = [abs(x[2]) for x in bound]

    # Tilt factor obtained experimentally, may not be great
    for i in outlier_indices(tilts, 1.743):
        bound[i] = bad_triangle

    # Verify our triangles exist in the correct octants
    h2, w2 = [x/4 for x in gray.shape[:2]]
    if bound[0][0][0] > w2 or bound[0][0][1] > h2:
        bound[0] = bad_triangle
    if bound[1][0][0] < w2*2 or bound[1][0][1] > h2:
        bound[1] = bad_triangle
    if bound[2][0][0] > w2 or bound[2][0][1] < h2*2:
        bound[2] = bad_triangle
    if bound[3][0][0] < w2*2 or bound[3][0][1] < h2*2:
        bound[3] = bad_triangle

    # Flip if 3 or more triangles are inverted
    inverted_triangles = 0
    for triangle in bound:
        if len(triangle) > 2 and triangle[3] is True:
            inverted_triangles += 1
    if inverted_triangles > 1:
        bound.reverse()

    if not any([True for x in bound if x==bad_triangle]):
        # Finally verify that all triplets form right-angle (ish) triangles
        # This whole bit is fairly nasty, but theoretically helps
        # invalid triangles that are incorrectly 
        # May be useless
        appears_in_right_angle = [False, False, False, False]
        vectors = []
        # Disgusting
        border_triangles = [
            [0, 1, 2], # lt, rt, lb
            [1, 0, 3], # rt, lt, rb
            [2, 3, 0], # lb, rb, lt
            [3, 1, 2], # rb, rt, lb
        ]
        for cycle in border_triangles:
            vectors.append(
                (
                    np.array(bound[cycle[1]][0]) - np.array(bound[cycle[0]][0]),
                    np.array(bound[cycle[2]][0]) - np.array(bound[cycle[0]][0])
                )
            )
        for i, v in enumerate(vectors):
            angle = angle_between(*v)
            if angle < 0.08 and angle > -0.08:
                for x in border_triangles[i]:
                    appears_in_right_angle[x] = True 
        
        bound = [
            x if appears_in_right_angle[i] else bad_triangle
            for i, x in enumerate(bound)
        ]

    rect =  np.array([b[0] for b in bound], dtype="float32")
    cv2.polylines(gray, [np.int32(rect.reshape((-1, 1, 2)))], True, (0,0,0), 2)
    cv2.imwrite('t2/dump/GGG.png', gray)
    return rect


class ImageMatcher:
    def __init__(self, source_img, aoi_data):
        # TODO open file if we get a path instead of a dict
        self.aoi_data = aoi_data

        # print('Loading source img')
        # TODO open file if we get a path instead of an image
        im_gray = cv2.cvtColor(source_img, cv2.COLOR_BGR2GRAY)
        # print('worked')

        height, width = im_gray.shape[:2]
        self.output_size = (width, height)

        # TODO -- make this just require triangle locations
        # Really need todo this, getting super slow....
        self.bounding_rect = get_bounding_rect(im_gray)

    def match(self, image):
        # TODO open file if we get a path instead of an image
        # We're switching from circles to triangles baby

        height, width = image.shape[:2]
        if height < width:
            # rotate sideways image
            image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
            height, width = image.shape[:2]
        color = (253, 253, 253)
        t = width * 0.1

        # Cut off 1/10 corners...
        corner_tris = [
            np.array([(0, 0), (0, t), (t, 0)], dtype=np.int32),
            np.array([(width, 0), (width-t, 0), (width, t)], dtype=np.int32),
            np.array([(0, height), (0, height-t), (t, height)], dtype=np.int32),
            np.array([(width, height), (width-t, height), (width, height-t)], dtype=np.int32)
        ]
        cv2.drawContours(image, corner_tris, -1, color, -1)

        # Convert our image to B/W before trying to orient it
        scan = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        scan = cv2.GaussianBlur(scan, (5, 5), cv2.BORDER_DEFAULT)
        scan = cv2.threshold(scan, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        bounding_rect = get_bounding_rect(scan)

        # Determine if we're missing any triangles from the image...
        bad_triangle_indices = []
        for i, t in enumerate(bounding_rect):
            if sum(t) == -2:
                bad_triangle_indices.append(i)
        if len(bad_triangle_indices) > 1:
            # cv2.imwrite("/home/m5/Scratch/TEST.png", im_gray)
            raise Exception("Too many bad tracking triangles. Bailing")
        # If we have ONE bad triangle, use AFFINE
        elif len(bad_triangle_indices) == 1:
            orig_rect = np.delete(
                self.bounding_rect, bad_triangle_indices[0],
                0
            )
            new_rect = np.delete(bounding_rect, bad_triangle_indices[0], 0)
            warp = cv2.getAffineTransform(new_rect, orig_rect)
            out = cv2.warpAffine(image, warp, self.output_size)
        # No bad triangles, use PERSPECTIVE
        else:
            warp = cv2.getPerspectiveTransform(
                bounding_rect,
                self.bounding_rect
            )
            out = cv2.warpPerspective(image, warp, self.output_size)
        return out

    def extract(self, image):
        match = self.match(image)
        return AOICollection(match, self.aoi_data, root=True)
