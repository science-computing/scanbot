import os
import mimetypes
try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse
from os.path import basename

import weasyprint


def px(pt):
    '''Convert pt to pixels using. Defaults to resolution of 288'''
    return int(4*pt)


def dict_to_style(d):
    '''Flatten a dictionary into a CSS style string'''
    style = ';'.join([
        '{}:{}'.format(key, str(val))
        for key, val in d.items()
    ])
    if len(style) > 0:
        style += ';'
    return style


def make_div(content, x, y, w, h, style={}, classes=''):
    '''Returns a standardized DIV element with x, y, h, w, and style'''
    return (
        '<div class="{}" '
        'style="{} left:{}pt; top:{}pt; width:{}pt; height:{}pt;">'
        '{}'
        '</div>').format(
        classes,
        dict_to_style(style),
        x, y, w, h,
        content
        )


def generate_url_fetcher(base_path=None):
    ''' Uses specified folder as base_path for file: images'''
    if base_path is None:
        base_path = os.getcwd()

    def fetcher(url):
        if url.startswith('file:'):
            mime_type, encoding = mimetypes.guess_type(url)
            url_path = url.replace('file://', '')
            data = {
                'mime_type': mime_type,
                'encoding': encoding,
                'filename': basename(url_path),
            }
            data['file_obj'] = open(os.path.join(base_path, url_path), 'rb')
            return data
        return weasyprint.default_url_fetcher(url)
    return fetcher

