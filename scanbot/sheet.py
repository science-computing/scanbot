from weasyprint import HTML, CSS
from .utils import generate_url_fetcher
from .matcher import ImageMatcher
from scanbot.components.MCQuestion import MCQuestionGrid, MCQuestion
from scanbot.components.NumberInput import NumberInput
from scanbot.components.HTMLBlock import HTMLBlock
from scanbot.mark_result import MarkResult
import numpy as np
import cv2
import io
import base64




sourceCSS = '''
    @page{{
        size: {};
        margin: 0;
    }}
    body{{
        font-size: 10pt;
        background-color: white;
    }}
    div{{
        position: fixed;
        overflow: hidden;
        box-sizing: border-box;
    }}

'''

component_map = {
    'MCQuestion': MCQuestion,
    'MCQuestionGrid': MCQuestionGrid,
    'HTMLBlock': HTMLBlock,
    'NumberInput': NumberInput,
}

def make_marker(marker):
    x, y, s = marker
    content = base64.b64encode(
        f'<svg height="{s}" width="{s}">'
        f'<polygon points="0,0 {s},0 {s/2},{s}" fill="black"/>'
        f'</svg>'.encode('utf-8')
    ).decode('utf-8')
    html = (
        f'<img style="position: fixed; top: {y}pt; left: {x}pt;" '
        f'src="data:image/svg+xml;base64,{content}">'
    )
    return html



class SheetTemplate:
    def __init__(self, config, base_path=None):
        self.css = sourceCSS.format(
            config.get('page_size', 'letter')
        )
        self.base_path = base_path

        #Bitch if we have bad markers

        self.markers = config.get('markers')
        self.components = []
        for k, component in config.get('components', dict()).items():
            component_class = component_map[component['type']]
            # Create the components
            self.components.append(
                component_class(
                    k,
                    component['x'],
                    component['y'],
                    component.get('config', dict())
                )
            )

    @property
    def html(self):
        if not hasattr(self, '_html'):
            self._html = ''.join([x.html for x in self.components])
            ## Append 4 marker divs
            for marker in self.markers:
                self._html += make_marker(marker)
        return self._html

    @property
    def aoi(self):
        if not hasattr(self, '_aoi'):
            self._aoi = {}
            for x in self.components:
                self._aoi.update(x.aoi)
        return self._aoi

    def gen_pdf(self, file_object, base_path=None):
        if not base_path:
            base_path = self.base_path
        HTML(
            string=self.html,
            url_fetcher=generate_url_fetcher(base_path)
        ).write_pdf(
            file_object,
            stylesheets=[CSS(string=self.css)],
        )

    def gen_ref_img(self, file_object, base_path=None):
        if not base_path:
            base_path = self.base_path
        HTML(
            string=self.html,
            url_fetcher=generate_url_fetcher(base_path)
        ).write_png(
            file_object,
            stylesheets=[CSS(string=self.css)],
            resolution=288,
        )

    def extract_data(self, image):
        if not hasattr(self, '_matcher'):
            # Build the matcher...
            # First we need an image in memory
            img = io.BytesIO()
            self.gen_ref_img(img)
            img.seek(0)
            # Convert to a cv2 image
            img = cv2.imdecode(np.fromstring(img.read(), np.uint8), cv2.IMREAD_COLOR)
            self._matcher = ImageMatcher(img, self.aoi)
        return self._matcher.extract(image)

    def mark(self, image):
        data = self.extract_data(image)
        return MarkResult(data, self.components)
