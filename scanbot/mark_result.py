import cv2


class MarkResult:
    def __init__(self, data, components):
        self.image = data.image
        self.marks = {}
        self.extracted = {}
        for x in components:
            mark, extract = x.mark(data)
            self.marks.update(mark)
            self.extracted.update(extract)

    def save_image(self, path):
        cv2.imwrite(path, self.image)

    def save_extracts(self, path, root):
        pass
