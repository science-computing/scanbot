import os
from setuptools import find_packages, setup
with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()
# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))
setup(
    name='scanbot',
    version='0.3',
    packages=find_packages(),
    scripts=[
        'bin/scanbot_gen_sheet',
        'bin/scanbot_extract',
        'bin/scanbot_validate',
    ],
    install_requires=[
        'WeasyPrint==48',
        'numpy>=1.17',
        'opencv-python-headless>=4.1.1.26'
    ],
    include_package_data=True,
    license='None',
    description='Minimal Suite for Authoring and Scanning bubble sheets',
    long_description=README,
    url='https://git.uwaterloo.ca/science-computing/scanbot',
    author='Mirko Vucicevich',
    author_email='mvucicev@uwaterloo.ca',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: Unlicense',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.7',
    ],
)
