Markbox Config Explorer
=============================

This example folder contains several configs derived from the current (as of 2020-03-03) Markbox. Each one will currently have an arbitrary number of questions

- To generate a PDF version of the sheet (for printing) you can run

- To generate an image (png) version of the sheet, you can run

- To run a test analysis on files, run the local script
