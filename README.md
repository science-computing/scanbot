A bubble sheet generation and recognition tool. Designed to be used with Markbox

## Requirements

Scanbot requires python version >= 3.7

The following will be required by weasyprint for generating images:

```
sudo apt-get install build-essential python3-dev python3-pip python3-setuptools python3-wheel python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info libpng12-0
```

or perhaps:

```
apt install python3-pip python3-cairo python3-gi-cairo pango1.0-tools
```


## Developing Scanbot

To quickly get started developing scanbot, ensure the above requirements are installed, then run the following:

```bash
# Get scanbot
git clone ist-git@git.uwaterloo.ca:science-computing/scanbot.git
cd scanbot

# Create a virtual environment
python3.7 -m venv env
source env/bin/activate
pip install --upgrade pip

# Install scanbot & requirements
pip install -e .
```

This should leave you in the scanbot virtual env with the ability to use scanbot's limited commandline tools for testing.


To run a quick test of scanbot, we can use the Markbox Example folder, which contains some useful configs:

```bash
cd examples/markbox_configs
scanbot_gen_
```
